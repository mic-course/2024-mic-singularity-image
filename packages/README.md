Please download the source packages of HLA-HD and netMHCpan from their website and put them in this folder,
and change the version numbers in the `Singularity.def` file accordingly, if necessary.

- HLA-HD: https://w3.genome.med.kyoto-u.ac.jp/HLA-HD/
- NetMHCpan: https://services.healthtech.dtu.dk/services/NetMHCpan-4.1/

