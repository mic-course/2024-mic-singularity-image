IMAGEDIR=/opt/apps/containers/community/mic-2024/dev

# get the sif build on gitlab
singularity pull \
  --force --dir ${IMAGEDIR} \
  oras://gitlab-registry.oit.duke.edu/mic-course/2024-mic-singularity-image:latest

wd=${IMAGEDIR}
sif=${IMAGEDIR}/2024-mic-singularity-image_latest.sif
flags="--containall --no-home --bind ${wd} ${sif}"

# basics
singularity exec ${flags} bash --version # 5.0.17
singularity exec ${flags} R --version # 4.2.2
singularity exec ${flags} python --version # 3.8.10
singularity exec ${flags} java -version # 17.0.9
singularity exec ${flags} awk -W version #5.0.1

# python-based software, installed through pip3 or setup.py
#none

# R
#singularity exec ${flags} R


# stand-alone software
# note: java-based software, use single quotes for bash -c!!
singularity exec --app htslib ${flags} tabix --version # 1.17
singularity exec --app samtools ${flags} samtools --version # 1.17
singularity exec ${flags} which samtools # global
singularity exec --app bedtools ${flags} bedtools --version # 2.31.0
singularity exec --app bwa ${flags} bwa #0.7.17-r1188

singularity exec --app gatk ${flags} gatk --version # 4.5.0.0
singularity exec --app picardtools ${flags} bash -c 'java -jar $PICARD_JAR CollectRnaSeqMetrics --version' # 3.0.0
singularity exec --app seqtk ${flags} seqtk # 1.3-r106
singularity exec --app bcftools ${flags} bcftools --version # 1.17
singularity exec --app ucsctools ${flags} genePredToBed
singularity exec --app sratoolkit ${flags} vdb-config 
singularity exec --app sratoolkit ${flags} fastq-dump
singularity exec --app bedops ${flags} convert2bed --version # 2.4.40
singularity exec --app vcftools ${flags} vcftools --version # 0.1.

