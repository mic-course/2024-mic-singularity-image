---
title: "Tool Test"
author: Tyler Schappe
date: 4/28/22
output: 
  html_document:
    keep_md: TRUE
---

**Goal:** The goal of this file is simply to test whether certain complex/large tools are available in the container. 

## R Packages

```{r}
library(tidyverse)
mylibs <- c('markdown','tidyverse','R.utils','rentrez','gsalib','reshape', 
'gplots','BiocManager','GEOquery')
lapply(mylibs, library, character.only = TRUE)

print_packageVersion <- function(lib){
  paste(lib, packageVersion(lib))
}
lapply(mylibs, print_packageVersion)
#[1] "markdown 1.12"
#[1] "tidyverse 2.0.0"
#[1] "R.utils 2.12.3"
#[1] "rentrez 1.2.3"
#[1] "gsalib 2.2.1"
#[1] "reshape 0.8.9"
#[1] "gplots 3.1.3.1"
#[1] "BiocManager 1.30.22"
#[1] "GEOquery 2.66.0"

```



## Bash

```{bash}
fastq-dump
```



