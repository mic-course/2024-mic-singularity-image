# 2024 MIC Rstudio Singularity Image

Singularity image for 2024 MIC Course

## Notes

### Building Image
```
export IMAGEDIR=/work/${USER}/images
mkdir -p $IMAGEDIR
srun -A chsi -p chsi -c10 --mem 20G apptainer build ${IMAGEDIR}/2024mic.sif Singularity.def
```

### Pulling Image
Below **TAG_NAME** should be replaced with the image tag you want to pull, for example, **latest** or **v0001**

#### Pulling into community image directory
```
srun --mem=20G -c 10 -A chsi -p chsi apptainer pull --force --dir /opt/apps/containers/community/mic-2024 oras://gitlab-registry.oit.duke.edu/mic-course/2024-mic-singularity-image:latest
```
